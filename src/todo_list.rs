extern crate uuid;

use self::uuid::Uuid;
use std::time::Duration;
use std::{io, process};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Todo {
    pub uuid: Uuid,
    pub name: String,
    pub complete: bool,
    pub duration: Option<Duration>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct TodoList {
    pub todos: Vec<Todo>,
}

impl TodoList {
    pub fn add(&mut self, name: &String, duration: Option<Duration>) {
        self.todos.push(Todo {
            uuid: Uuid::new_v4(),
            name: String::from(name),
            complete: false,
            duration,
        });
    }

    fn format_duration(duration: Option<Duration>) -> String {
        if let Some(dur) = duration {
            let secs = dur.as_secs();
            if secs % 3600 == 0 {
                format!("({}h)", secs / 3600)
            } else {
                format!("({}m)", secs / 60)
            }
        } else {
            String::from("")
        }
    }

    pub fn list(&mut self, writer: &mut dyn io::Write) {
        let incomplete_todos = self.todos.iter().filter(|todo| !todo.complete);
        let count = incomplete_todos.clone().count();
        let number_width = (((count + 1) as f64).log10().floor() as usize) + 1;
        let name_width = incomplete_todos
            .clone()
            .map(|t| t.name.chars().count())
            .max()
            .unwrap_or(0);

        if count == 0 {
            self.print_ln("There are no TODOs in the list", writer);
        } else {
            self.print_ln(&format!("There are {} TODOs:", count), writer);
            self.print_ln("", writer);
        }

        for (i, todo) in incomplete_todos.enumerate() {
            self.print_ln(
                &format!(
                    "{:number_width$}: {:name_width$} {}",
                    i + 1,
                    todo.name,
                    Self::format_duration(todo.duration),
                    number_width = number_width,
                    name_width = name_width
                ),
                writer,
            );
        }
    }

    pub fn complete(&mut self, id: usize) {
        let mut incomplete_todos = self.todos.iter_mut().filter(|todo| !todo.complete);

        if let Some(todo) = incomplete_todos.nth(id - 1) {
            todo.complete = true;
        } else {
            eprintln!("ERROR: There is no TODO with ID {}", id);
            process::exit(1);
        }
    }

    pub fn delete(&mut self, id: usize) {
        let mut incomplete_todos = self.todos.iter().filter(|todo| !todo.complete);

        if let Some(todo) = incomplete_todos.nth(id - 1) {
            let mut index: usize = 0;
            for (i, itodo) in self.todos.iter().enumerate() {
                if itodo.uuid == todo.uuid {
                    index = i;
                    break;
                }
            }
            self.todos.remove(index);
        } else {
            eprintln!("ERROR: There is no TODO with ID {}", id);
            process::exit(1);
        }
    }

    pub fn edit(&mut self, id: usize, duration: Option<Duration>) {
        let mut incomplete_todos = self.todos.iter_mut().filter(|todo| !todo.complete);

        if let Some(todo) = incomplete_todos.nth(id - 1) {
            if let Some(dur) = duration {
                todo.duration = Some(dur);
            }
        } else {
            eprintln!("ERROR: There is no TODO with ID {}", id);
            process::exit(1);
        }
    }

    fn print_ln(&self, line: &str, writer: &mut dyn io::Write) {
        writer.write_all(line.as_bytes()).unwrap();
        writer.write_all("\n".as_bytes()).unwrap();
    }
}

#[cfg(test)]
mod tests {
    extern crate tempfile;

    use super::*;

    #[test]
    fn adds_a_new_todo() {
        let mut todo_list = TodoList {
            todos: vec![Todo {
                uuid: Uuid::parse_str("550e8400-e29b-41d4-a716-446655440000").unwrap(),
                name: String::from("First TODO"),
                complete: false,
                duration: None,
            }],
        };

        todo_list.add(
            &String::from("This is a new TODO"),
            Some(Duration::from_secs(300)),
        );
        let new_todo = todo_list.todos.last().unwrap();

        assert_eq!(
            todo_list,
            TodoList {
                todos: vec![
                    Todo {
                        uuid: Uuid::parse_str("550e8400-e29b-41d4-a716-446655440000").unwrap(),
                        name: String::from("First TODO"),
                        complete: false,
                        duration: None,
                    },
                    Todo {
                        uuid: new_todo.uuid,
                        name: String::from("This is a new TODO"),
                        complete: false,
                        duration: Some(Duration::from_secs(300)),
                    },
                ],
            }
        )
    }

    #[test]
    fn list_prints_empty_list() {
        let mut test_writer = TestWriter::new();
        let mut todo_list = TodoList { todos: vec![] };

        todo_list.list(&mut test_writer);

        assert_eq!(test_writer.contents, "There are no TODOs in the list\n")
    }

    #[test]
    fn list_prints_incomplete_todos_and_durations() {
        let mut test_writer = TestWriter::new();
        let mut todo_list = TodoList {
            todos: vec![
                Todo {
                    uuid: Uuid::new_v4(),
                    name: String::from("First TODO"),
                    complete: false,
                    duration: None,
                },
                Todo {
                    uuid: Uuid::new_v4(),
                    name: String::from("Second TODO"),
                    complete: false,
                    duration: Some(Duration::from_secs(3600)),
                },
                Todo {
                    uuid: Uuid::new_v4(),
                    name: String::from("Third TODO"),
                    complete: true,
                    duration: None,
                },
                Todo {
                    uuid: Uuid::new_v4(),
                    name: String::from("Fourth TODO"),
                    complete: false,
                    duration: Some(Duration::from_secs(1800)),
                },
            ],
        };

        todo_list.list(&mut test_writer);

        assert_eq!(
            test_writer.contents,
            "There are 3 TODOs:\n\n1: First TODO  \n2: Second TODO (1h)\n3: Fourth TODO (30m)\n"
        )
    }

    #[test]
    fn complete_modifies_an_existing_todo() {
        let mut todo_list = TodoList {
            todos: vec![
                Todo {
                    uuid: Uuid::parse_str("1fc6594e-a4c9-4671-95a3-8bb01ca75286").unwrap(),
                    name: String::from("First TODO"),
                    complete: true,
                    duration: None,
                },
                Todo {
                    uuid: Uuid::parse_str("5e951088-0a7a-4343-8c7b-5a77c2ff15c8").unwrap(),
                    name: String::from("Second TODO"),
                    complete: false,
                    duration: None,
                },
                Todo {
                    uuid: Uuid::parse_str("f1712f67-69d2-4a3b-993e-ed2bb170adae").unwrap(),
                    name: String::from("Third TODO"),
                    complete: false,
                    duration: None,
                },
            ],
        };

        todo_list.complete(1);

        assert_eq!(
            todo_list,
            TodoList {
                todos: vec![
                    Todo {
                        uuid: Uuid::parse_str("1fc6594e-a4c9-4671-95a3-8bb01ca75286").unwrap(),
                        name: String::from("First TODO"),
                        complete: true,
                        duration: None,
                    },
                    Todo {
                        uuid: Uuid::parse_str("5e951088-0a7a-4343-8c7b-5a77c2ff15c8").unwrap(),
                        name: String::from("Second TODO"),
                        complete: true,
                        duration: None,
                    },
                    Todo {
                        uuid: Uuid::parse_str("f1712f67-69d2-4a3b-993e-ed2bb170adae").unwrap(),
                        name: String::from("Third TODO"),
                        complete: false,
                        duration: None,
                    },
                ],
            }
        )
    }

    #[test]
    fn deletes_an_incomplete_todo() {
        let mut todo_list = TodoList {
            todos: vec![
                Todo {
                    uuid: Uuid::parse_str("2ad39386-f061-4ce8-a7c1-46bd364062a6").unwrap(),
                    name: String::from("First TODO"),
                    complete: true,
                    duration: None,
                },
                Todo {
                    uuid: Uuid::parse_str("728cacb8-d5b5-4c80-8572-e14e7e83105e").unwrap(),
                    name: String::from("Second TODO"),
                    complete: false,
                    duration: None,
                },
                Todo {
                    uuid: Uuid::parse_str("a43c1305-05e0-4a12-8408-b2aa816f501e").unwrap(),
                    name: String::from("Third TODO"),
                    complete: false,
                    duration: None,
                },
            ],
        };

        todo_list.delete(1);

        assert_eq!(
            todo_list,
            TodoList {
                todos: vec![
                    Todo {
                        uuid: Uuid::parse_str("2ad39386-f061-4ce8-a7c1-46bd364062a6").unwrap(),
                        name: String::from("First TODO"),
                        complete: true,
                        duration: None,
                    },
                    Todo {
                        uuid: Uuid::parse_str("a43c1305-05e0-4a12-8408-b2aa816f501e").unwrap(),
                        name: String::from("Third TODO"),
                        complete: false,
                        duration: None,
                    },
                ]
            }
        )
    }

    #[test]
    fn edits_an_incomplete_todo() {
        let mut todo_list = TodoList {
            todos: vec![
                Todo {
                    uuid: Uuid::parse_str("b558548c-6ef9-4928-a7dd-7c30600f2da0").unwrap(),
                    name: String::from("First TODO"),
                    complete: true,
                    duration: None,
                },
                Todo {
                    uuid: Uuid::parse_str("e4033310-d5f1-437f-83f2-5c85ff172050").unwrap(),
                    name: String::from("Second TODO"),
                    complete: false,
                    duration: None,
                },
            ],
        };

        todo_list.edit(1, Some(Duration::from_secs(30 * 60)));

        assert_eq!(
            todo_list,
            TodoList {
                todos: vec![
                    Todo {
                        uuid: Uuid::parse_str("b558548c-6ef9-4928-a7dd-7c30600f2da0").unwrap(),
                        name: String::from("First TODO"),
                        complete: true,
                        duration: None,
                    },
                    Todo {
                        uuid: Uuid::parse_str("e4033310-d5f1-437f-83f2-5c85ff172050").unwrap(),
                        name: String::from("Second TODO"),
                        complete: false,
                        duration: Some(Duration::from_secs(30 * 60)),
                    },
                ]
            }
        )
    }

    struct TestWriter {
        contents: String,
    }

    impl TestWriter {
        fn new() -> Self {
            TestWriter {
                contents: String::new(),
            }
        }
    }

    impl io::Write for TestWriter {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            self.contents.push_str(std::str::from_utf8(buf).unwrap());

            Ok(buf.len())
        }

        fn flush(&mut self) -> io::Result<()> {
            Ok(())
        }
    }
}
