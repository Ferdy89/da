use regex::Regex;
use std::env;
use std::time::Duration;

pub enum Command {
    Add {
        name: String,
        duration: Option<Duration>,
    },
    List,
    Complete {
        id: usize,
    },
    Delete {
        id: usize,
    },
    Edit {
        id: usize,
        duration: Option<Duration>,
    },
}

pub struct ArgList {
    args: Vec<String>,
}

impl ArgList {
    pub fn from_process() -> Self {
        Self {
            args: env::args().skip(1).collect(),
        }
    }

    pub fn to_command<'a>(&self) -> Result<Command, &'a str> {
        let mut args_iter = self.args.iter();

        if let Some(key) = args_iter.next() {
            match key.as_str() {
                "add" | "a" => ArgList::parse_add(args_iter),
                "list" | "l" => ArgList::parse_list(args_iter),
                "complete" | "c" => ArgList::parse_complete(args_iter),
                "delete" | "d" => ArgList::parse_delete(args_iter),
                "edit" | "e" => ArgList::parse_edit(args_iter),
                _ => Err("Command not recognized"),
            }
        } else {
            Err("Needs to be called with a command")
        }
    }

    fn parse_add<'a>(mut args_iter: std::slice::Iter<String>) -> Result<Command, &'a str> {
        let mut name: Result<&str, &str> = Err("To add a TODO you need to specify a name");
        let mut duration: Option<Duration> = None;

        while let Some(arg) = args_iter.next() {
            match arg.as_str() {
                "-d" => duration = Some(ArgList::parse_duration(&mut args_iter)?),
                name_arg => {
                    if let Ok(_) = name {
                        return Err("The add command takes only 1 name");
                    }
                    name = Ok(name_arg);
                }
            }
        }

        Ok(Command::Add {
            name: String::from(name?),
            duration,
        })
    }

    fn parse_list<'a>(mut args_iter: std::slice::Iter<String>) -> Result<Command, &'a str> {
        match args_iter.next() {
            Some(_) => Err("The list command doesn't take any arguments"),
            None => Ok(Command::List),
        }
    }

    fn parse_complete<'a>(mut args_iter: std::slice::Iter<String>) -> Result<Command, &'a str> {
        if let Some(str_id) = args_iter.next() {
            let id = ArgList::parse_id(str_id)?;
            match args_iter.next() {
                Some(_) => Err("The complete command takes only 1 argument"),
                None => Ok(Command::Complete { id }),
            }
        } else {
            Err("To complete a TODO you need to specify an ID")
        }
    }

    fn parse_delete<'a>(mut args_iter: std::slice::Iter<String>) -> Result<Command, &'a str> {
        if let Some(str_id) = args_iter.next() {
            let id = ArgList::parse_id(str_id)?;
            match args_iter.next() {
                Some(_) => Err("The delete command takes only 1 argument"),
                None => Ok(Command::Delete { id }),
            }
        } else {
            Err("To delete a TODO you need to specify an ID")
        }
    }

    fn parse_edit<'a>(mut args_iter: std::slice::Iter<String>) -> Result<Command, &'a str> {
        let mut id: Result<usize, &str> = Err("To edit a TODO you need to specify an ID");
        let mut duration: Option<Duration> = None;

        while let Some(arg) = args_iter.next() {
            match arg.as_str() {
                "-d" => duration = Some(ArgList::parse_duration(&mut args_iter)?),
                str_id => {
                    if let Ok(_) = id {
                        return Err("The edit command takes only 1 ID");
                    }
                    id = Ok(ArgList::parse_id(str_id)?);
                }
            }
        }

        Ok(Command::Edit { id: id?, duration })
    }

    fn parse_id<'a>(str_id: &str) -> Result<usize, &'a str> {
        str_id.parse().or(Err("ID must be a number"))
    }

    fn parse_duration<'a>(args_iter: &mut std::slice::Iter<String>) -> Result<Duration, &'a str> {
        if let Some(str_duration) = args_iter.next() {
            let duration_re = Regex::new(r"(?P<number>\d+)(?P<unit>[hm])").unwrap();
            if let Some(captures) = duration_re.captures(str_duration) {
                // Regex only admits hours and minutes, so it's either one or the other
                let factor = if &captures["unit"] == "h" { 3600 } else { 60 };
                let number: u64 = captures["number"].parse().unwrap();
                Ok(Duration::from_secs(number * factor))
            } else {
                Err("Duration must be expressed as a number and a unit (h = hours, m = minutes)")
            }
        } else {
            Err("Flag -d (duration) needs an argument")
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_command_takes_a_name() {
        let arg_list = build(vec!["add", "well well well"]);

        let command = arg_list.to_command().unwrap();

        match command {
            Command::Add { name, duration: _ } => assert_eq!(name, "well well well"),
            _ => assert!(false),
        }
    }

    #[test]
    fn add_command_takes_only_one_name() {
        let arg_list = build(vec!["add", "well", "well", "well"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn add_command_can_take_a_duration() {
        let arg_list = build(vec!["add", "do it!", "-d", "1h"]);

        let command = arg_list.to_command().unwrap();

        match command {
            Command::Add { name, duration } => {
                assert_eq!(name, "do it!");
                assert_eq!(duration, Some(Duration::from_secs(3600)));
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn add_command_name_might_be_last() {
        let arg_list = build(vec!["add", "-d", "1h", "do it!"]);

        let command = arg_list.to_command().unwrap();

        match command {
            Command::Add { name, duration } => {
                assert_eq!(name, "do it!");
                assert_eq!(duration, Some(Duration::from_secs(3600)));
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn list_command() {
        let arg_list = build(vec!["list"]);

        let command = arg_list.to_command().unwrap();

        match command {
            Command::List => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn list_command_accepts_no_parameters() {
        let arg_list = build(vec!["list", "more"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn complete_command() {
        let arg_list = build(vec!["complete", "5"]);

        let command = arg_list.to_command().unwrap();

        match command {
            Command::Complete { id } => assert_eq!(id, 5),
            _ => assert!(false),
        }
    }

    #[test]
    fn complete_command_takes_a_number() {
        let arg_list = build(vec!["complete", "foo"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn complete_command_takes_only_one_number() {
        let arg_list = build(vec!["complete", "2", "2"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn delete_command() {
        let arg_list = build(vec!["delete", "3"]);

        let command = arg_list.to_command().unwrap();

        match command {
            Command::Delete { id } => assert_eq!(id, 3),
            _ => assert!(false),
        }
    }

    #[test]
    fn delete_command_takes_a_number() {
        let arg_list = build(vec!["delete", "foo"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn delete_command_takes_only_one_number() {
        let arg_list = build(vec!["delete", "2", "2"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn edit_command() {
        let arg_list = build(vec!["edit", "1", "-d", "30m"]);

        let command = arg_list.to_command().unwrap();

        match command {
            Command::Edit { id, duration } => {
                assert_eq!(id, 1);
                assert_eq!(duration, Some(Duration::from_secs(30 * 60)));
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn edit_command_requires_an_id() {
        let arg_list = build(vec!["edit"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn edit_command_requires_a_numeric_id() {
        let arg_list = build(vec!["edit", "foo"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn edit_command_can_be_empty() {
        let arg_list = build(vec!["edit", "1"]);

        let command = arg_list.to_command().unwrap();

        match command {
            Command::Edit { id, duration } => {
                assert_eq!(id, 1);
                assert_eq!(duration, None);
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn edit_command_can_take_id_last() {
        let arg_list = build(vec!["edit", "-d", "1h", "2"]);

        let command = arg_list.to_command().unwrap();

        match command {
            Command::Edit { id, duration } => {
                assert_eq!(id, 2);
                assert_eq!(duration, Some(Duration::from_secs(3600)));
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn edit_command_takes_only_one_id() {
        let arg_list = build(vec!["edit", "1", "2"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn edit_command_duration_must_be_specified() {
        let arg_list = build(vec!["edit", "1", "-d"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn edit_command_duration_must_have_the_right_format() {
        let arg_list = build(vec!["edit", "1", "-d", "10s"]);

        let command = arg_list.to_command();

        match command {
            Err(_) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    fn build(args: Vec<&str>) -> ArgList {
        ArgList {
            args: args.iter().map(|s| s.to_string()).collect(),
        }
    }
}
