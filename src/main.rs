extern crate dirs;
#[macro_use]
extern crate serde_derive;
extern crate regex;
extern crate serde_yaml;

mod command;
mod local_storage;
mod todo_list;

use command::{ArgList, Command};
use std::{io, process};

const USAGE: &'static str = r#"
Usage: da <command> [<args>]

Developer Assistant (da) helps you manage your TODO list from the command line

Available commands:

  Command          | Short | Description
  -----------------|-------|------------
  add NAME         | a     | Adds a new TODO with name NAME
    -d NUMBER(h|m) |       |   Adds a duration to the TODO
  list             | l     | Lists all the TODOs
  complete ID      | c     | Marks TODO with number ID as complete
  delete ID        | d     | Deletes TODO with number ID
  edit ID          | e     | Edits TODO with number ID
    -d NUMBER(h|m) |       |   Changes the duration of the TODO
"#;

fn main() {
    let command = ArgList::from_process().to_command().unwrap_or_else(|err| {
        eprintln!("ERROR: {}", err);
        eprintln!("{}", USAGE);
        process::exit(1);
    });
    match local_storage::LocalStorage::on_default_path() {
        Ok(mut storage) => match storage.read() {
            Ok(mut todo_list) => {
                run(command, &mut todo_list);
                storage.write(todo_list).unwrap_or_else(graceful_exit);
            }
            Err(error) => graceful_exit(error),
        },
        Err(error) => graceful_exit(error),
    }
}

fn graceful_exit(error: String) {
    eprintln!("{}", error);
    process::exit(1);
}

fn run(command: Command, todo_list: &mut todo_list::TodoList) {
    match command {
        Command::Add { name, duration } => todo_list.add(&name, duration),
        Command::List => todo_list.list(&mut io::stdout()),
        Command::Complete { id } => todo_list.complete(id),
        Command::Delete { id } => todo_list.delete(id),
        Command::Edit { id, duration } => todo_list.edit(id, duration),
    }
}
