extern crate uuid;

use std::io::prelude::*;
use todo_list::TodoList;

const DEFAULT_FILENAME: &'static str = ".da.yaml";

pub struct LocalStorage {
    pub path: std::path::PathBuf,
}

impl LocalStorage {
    /// Reads the YAML from the default path in the user's home directory and turns it into a TodoList
    /// struct.
    pub fn on_default_path() -> Result<Self, String> {
        match dirs::home_dir() {
            Some(mut path) => {
                path.push(DEFAULT_FILENAME);
                Ok(Self { path: path })
            }
            None => Err(String::from(
                "ERROR: Could not locate the current user's home directory",
            )),
        }
    }

    /// Reads a YAML file from a given path and turns it into a TodoList struct.
    pub fn read(&mut self) -> Result<TodoList, String> {
        let mut contents = String::new();
        if let Err(error) = self.open_file(false)?.read_to_string(&mut contents) {
            return Err(format!("ERROR: Could not read file: {}", error));
        }

        self.todo_list_from_yaml(contents.as_str())
    }

    /// Writes a TodoList struct into a YAML file in the given path.
    pub fn write(&mut self, todo_list: TodoList) -> Result<(), String> {
        println!("{:?}", self.todo_list_to_yaml(&todo_list));
        match write!(
            self.open_file(true)?,
            "{}\n",
            self.todo_list_to_yaml(&todo_list)
        ) {
            Ok(_) => Ok(()),
            Err(error) => Err(format!("ERROR: Could not write to file: {}", error)),
        }
    }

    fn open_file(&mut self, for_writing: bool) -> Result<std::fs::File, String> {
        let open_attempt = if self.path.exists() {
            std::fs::OpenOptions::new()
                .read(true)
                .write(true)
                .truncate(for_writing)
                .open(&self.path)
        } else {
            std::fs::OpenOptions::new()
                .read(true)
                .write(true)
                .create_new(true)
                .open(&self.path)
        };

        open_attempt.or_else(|error| {
            Err(format!(
                "ERROR: Could not open file {:?} for writing: {}",
                self.path, error
            ))
        })
    }

    fn todo_list_from_yaml(&self, body: &str) -> Result<TodoList, String> {
        if body == "" {
            return Ok(TodoList { todos: vec![] });
        }

        serde_yaml::from_str(&body).or_else(|err| {
            Err(format!(
                "ERROR: File contains invalid YAML: {}",
                err.to_string()
            ))
        })
    }

    fn todo_list_to_yaml(&self, todo_list: &TodoList) -> String {
        serde_yaml::to_string(&todo_list).unwrap()
    }
}

#[cfg(test)]
mod tests {
    extern crate tempfile;

    use self::uuid::Uuid;
    use super::*;
    use std::io;
    use std::time::Duration;
    use todo_list::Todo;

    #[test]
    fn read_from_local_file() {
        let mut tempfile = tempfile::NamedTempFile::new().unwrap();
        let mut storage = LocalStorage {
            path: tempfile.path().to_path_buf(),
        };
        write!(
            tempfile,
            "---
todos:
  - uuid: 2379b43c-6150-4aba-9198-498e702dae2a
    name: First TODO
    complete: true
    duration:
      secs: 120
      nanos: 0
  - uuid: 5f09e799-81b2-4eb9-ac94-f73ff427db27
    name: Second TODO
    complete: false
"
        )
        .unwrap();

        match storage.read() {
            Ok(todo_list) => assert_eq!(
                todo_list,
                TodoList {
                    todos: vec![
                        Todo {
                            uuid: Uuid::parse_str("2379b43c-6150-4aba-9198-498e702dae2a").unwrap(),
                            name: String::from("First TODO"),
                            complete: true,
                            duration: Some(Duration::from_secs(120)),
                        },
                        Todo {
                            uuid: Uuid::parse_str("5f09e799-81b2-4eb9-ac94-f73ff427db27").unwrap(),
                            name: String::from("Second TODO"),
                            complete: false,
                            duration: None,
                        },
                    ],
                }
            ),
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn read_errors_if_path_does_not_exist() {
        let mut storage = LocalStorage {
            path: std::path::PathBuf::from("/file-that-does-not-exist"),
        };
        match storage.read() {
            Ok(_) => assert!(false),
            Err(error) => assert!(
                error.contains("Could not open file \"/file-that-does-not-exist\" for writing")
            ),
        }
    }

    #[test]
    fn read_errors_if_file_cannot_be_read() {
        let mut tempfile = tempfile::NamedTempFile::new().unwrap();
        let mut storage = LocalStorage {
            path: tempfile.path().to_path_buf(),
        };
        let invalid_utf8_stream = [245]; // 0xF5, invalid UTF-8 character
        tempfile.write(&invalid_utf8_stream).unwrap();

        match storage.read() {
            Ok(_) => assert!(false),
            Err(error) => assert!(error.contains("Could not read file")),
        }
    }

    #[test]
    fn read_errors_if_file_has_invalid_yaml() {
        let mut tempfile = tempfile::NamedTempFile::new().unwrap();
        let mut storage = LocalStorage {
            path: tempfile.path().to_path_buf(),
        };
        write!(
            tempfile,
            "---
Not
Valid
YAML
"
        )
        .unwrap();

        match storage.read() {
            Ok(_) => assert!(false),
            Err(error) => assert!(error.contains("File contains invalid YAML")),
        }
    }

    #[test]
    fn write_to_local_file() {
        let mut tempfile = tempfile::NamedTempFile::new().unwrap();
        let mut storage = LocalStorage {
            path: tempfile.path().to_path_buf(),
        };
        let todo_list = TodoList {
            todos: vec![
                Todo {
                    uuid: Uuid::parse_str("c3b21694-be70-4374-9fa4-ed8851989cea").unwrap(),
                    name: String::from("First TODO"),
                    complete: true,
                    duration: Some(Duration::from_secs(60)),
                },
                Todo {
                    uuid: Uuid::parse_str("bafeb2f4-c579-4bae-a9d8-de1c609483ab").unwrap(),
                    name: String::from("Second TODO"),
                    complete: false,
                    duration: None,
                },
            ],
        };

        storage.write(todo_list).unwrap();

        let mut contents = String::new();
        tempfile.seek(io::SeekFrom::Start(0)).unwrap();
        tempfile.read_to_string(&mut contents).unwrap();
        assert_eq!(
            contents,
            "---
todos:
  - uuid: c3b21694-be70-4374-9fa4-ed8851989cea
    name: First TODO
    complete: true
    duration:
      secs: 60
      nanos: 0
  - uuid: bafeb2f4-c579-4bae-a9d8-de1c609483ab
    name: Second TODO
    complete: false
    duration: ~
"
        );
    }

    #[test]
    fn write_errors_if_file_does_not_exist() {
        let mut storage = LocalStorage {
            path: std::path::PathBuf::from("/file-that-does-not-exist"),
        };
        let todo_list = TodoList { todos: vec![] };

        match storage.write(todo_list) {
            Ok(_) => assert!(false),
            Err(error) => assert!(
                error.contains("Could not open file \"/file-that-does-not-exist\" for writing")
            ),
        }
    }
}
