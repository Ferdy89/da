# Developer Assistant (da)

`da` is a simple tool to help you track your TODOs from the command line.

If you spend a good amount of your time in your terminal, you might enjoy not
having to use your mouse for frequent actions like keeping a TODO list. If
you're like me, you might keep a text file with those TODOs, which is what `da`
does anyway!

## Usage

```sh
# Add TODOs
da add Start the project
da a Finish the project

# Browse your TODOs
da list # => Equivalent to `da l`
```

## Contributing

I began this project to learn the Rust language while doing something that might
be useful to me. If it's useful for you and you have any ideas, I would love if
you could open an issue to tell me about it.

Please note that this project is released with a [Contributor Code of
Conduct](CODE_OF_CONDUCT.md).  By participating in this project you agree to
abide by its terms.

### Release

A release means to establish an API that users can rely on and publishing the
different binaries on GitLab.

First create a git tag with the version being released:

```sh
git tag --add v0.1
```

Write a useful description describing what's changed since the last release,
then push the tag. Edit the tag [from the web
interface](https://gitlab.com/Ferdy89/da/tags) and add the binaries generated
locally under `target/release`.
